CSE.30341.FA17: Project 06
==========================

This is the documentation for [Project 06] of [CSE.30341.FA17].

Members
-------

1. Michael Wang (mwang6@nd.edu)
2. Donald Luc (dluc@nd.edu)
3. Anthony Luc (aluc@nd.edu)

Design
------

> 1. To implement `Filesystem::debug`, you will need to load the file system
>    data structures and report the **superblock** and **inodes**.
>
>       - How will you read the superblock?
We will read the superblock by calling the public function read for the Disk class and declaring the block number as 0 for the first argument.
>       - How will you traverse all the inodes?
You can traverse all the inodes by calling the public function read for the File System class and looping through each inode block and reading each inode within each block if it is valid.
>       - How will you determine all the information related to an inode?
We will call Filesystem::read to read from inode.
>       - How will you determine all the blocks related to an inode?
We will take the number of direct and indirect pointers to a block. 

> 2. To implement `FileSystem::format`, you will need to write the superblock
>    and clear the remaining blocks in the file system.
>
>       - What pre-condition must be true before this operation can succeed?
We have to check if the disk is not mounted, then we can do Filesystem::format, otherwise return failure.
>       - What information must be written into the superblock?
We must write the magic number, number of blocks in the filesystem, inode blocks, and inodes. 10% of the blocks must be set aside for inodes.
>       - How would you clear all the remaining blocks?
We will set all the remaining blocks as plain data blocks.


> 3. To implement `FileSystem::mount`, you will need to prepare a filesystem
>    for use by reading the superblock and allocating the free block bitmap.
>
>       - What pre-condition must be true before this operation can succeed?
We first need to check if there is a disk on the filesystem, if one is present, then we can read the superblock. 
>       - What sanity checks must you perform?
The sanity checks we will perform are to check if the blocknum is within the range of the current number of blocks within the disk. We also have to check if the data we got was valid.
>       - How will you record that you mounted a disk?
We will return true if all the functions pass. 
>       - How will you determine which blocks are free?
We will go through the free block bitmap check whether a block is free or not.


> 4. To implement `FileSystem::create`, you will need to locate a free inode
>    and save a new inode into the inode table.
>
>       - How will you locate a free inode?
We will go through the inode blocks and if the inode valid bit is 0 then we know it is free.
>       - What information would you see in a new inode?
We will see whether it is valid, the size of the file, and the direct and indirect pointers.
>       - How will you record this new inode?
We will set valid equal to 0, size equal to 0, the direct would be instantiated with how many pointers per inode there are, and the indirect pointer would be set to null.


> 5. To implement `FileSystem::remove`, you will need to locate the inode and
>    then free its associated blocks.
>
>       - How will you determine if the specified inode is valid?
We will check the valid bit variable for that inode.
>       - How will you free the direct blocks?
We will iterate through the direct blocks, free what is necessary, and then return it to the free block bitmap.
>       - How will you free the indirect blocks?
We will iterate through the indirect blocks, free what is necessary, and then return it to the free block bitmap.
>       - How will you update the inode table?
We will go through the inode blocks and delete the inode associated with the inumber.


> 6. To implement `FileSystem::stat`, you will need to locate the inode and
>    return its size.
>
>       - How will you determine if the specified inode is valid?
We will check the valid bit to see if it 1.
>       - How will you determine the inode's size?
We will return the size variable within the inode struct.


> 7. To implement `FileSystem::read`, you will need to locate the inode and
>    copy data from appropriate blocks to the user-specified data buffer.
>
>       - How will you determine if the specified inode is valid?
We will check the valid bit to see if it 1.
>       - How will you determine which block to read from?
We will start at offset given to us in the inode to determine which block to read from.
>       - How will you handle the offset?
If offset is between 0 and POINTERS_PER_INODE - 1 then we access the Direct array, else we will take the Indirect pointer and access the Indirect Block.
>       - How will you copy from a block to the data buffer?
We will use memcopy to copy the data in the block to the data buffer.



> 8. To implement `FileSystem::write`, you will need to locate the inode and
>    copy data the user-specified data buffer to data blocks in the file
>    system.
>
>       - How will you determine if the specified inode is valid?
We will check the valid bit to see if it 1.
>       - How will you determine which block to write to?
We will load in an inode and then based on the offset, we will either write to a direct or indirect block.
>       - How will you handle the offset?
If offset is between 0 and POINTERS_PER_INODE - 1 then we access the Direct array, else we will take the Indirect pointer and access the Indirect Block.
>       - How will you know if you need a new block?
If the length is greater than the size of 4KB, we need to create a new block.
>       - How will you manage allocating a new block if you need another one?
We will use another data block from the Direct[] array.
>       - How will you copy from a block to the data buffer?
We will use memcopy to copy the data buffer to the block.
>       - How will you update the inode?
We will reset the size of the inode and also the direct/indirect pointers.

Errata
------

> We passed all the first 6 tests either by running the tests or by manually checking the values. Usually our read and writes were off by a bit but this was assumed to be okay. Our copy out doesn't pass for 20 and 200 because of the indirect blocks and also copy in. Other than this we passed the other tests.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 06]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project06.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
